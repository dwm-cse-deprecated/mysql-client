#! /bin/bash

set -ex

yum -y install wget

MYSQL_CLIENT_RPM="https://dev.mysql.com/get/Downloads/MySQL-5.6/MySQL-client-5.6.29-1.linux_glibc2.5.x86_64.rpm"
MYSQL_DEVEL_RPM="https://dev.mysql.com/get/Downloads/MySQL-5.6/MySQL-devel-5.6.29-1.linux_glibc2.5.x86_64.rpm"
MYSQL_SHARED_RPM="https://dev.mysql.com/get/Downloads/MySQL-5.6/MySQL-shared-compat-5.6.29-1.linux_glibc2.5.x86_64.rpm"

cd /usr/local/src
wget --connect-timeout 60 ${MYSQL_CLIENT_RPM}
wget --connect-timeout 60 ${MYSQL_DEVEL_RPM}
wget --connect-timeout 60 ${MYSQL_SHARED_RPM}

rpm -ivh MySQL-*-5.6.29-1.linux_glibc2.5.x86_64.rpm

